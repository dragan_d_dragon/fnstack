# README #

### What is this repository for? ###

* Quick presentation of function stack concept
* Version 1.0.0

### How do I get set up? ###

1. Get latest version of Node.js and npm.
2. Clone repo.
3. Run *npm install* in project's root.
4. Run *npm start* in project's root.
5. Visit *http://localhost:8080*
6. Open console to see the logs.
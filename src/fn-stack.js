module.exports = function(cfg/*we dont use it now, but just in case*/){

	var self = Object.create({get length () { return fnStack.length} });

	var fnStack = [];

	
	/**
	* 	Adds new function to function stack.
	*	Function should be not change state/arguments that receves, function should return true it wants to
	*   remain on the stack, otherwise it should return false.
	*/
	self.push = function(fn){

		if(typeof fn !== "function"){
			throw new TypeError("Only function can be pushed to function stack!");
		}

		else {
			fnStack.push(fn);
		}
	}

	/**
	* Executes function stack using FIFO ordering. Each function from stack is executed with arguments given to execiteStack.
	*/
	self.executeStack = function(/*poly*/){

		var args = arguments;

		newFnStack = fnStack.filter(function(fn){
			return fn.apply(null, args);
		});

		fnStack = newFnStack;
	}

	return self;

}
var fnStack =  require('./src/fn-stack.js')();

var fn1 = function(bool){ console.log('FN1 exec.. ', bool); return bool; }
var fn2 = function(bool1, bool2){ console.log('FN2 exec.. ', bool2); return bool2;}
var fn3 = function(bool1, bool2){ console.log('FN3 exec.. ', bool2); return bool1 || bool2;}
var fn4 = function(bool1, bool2){ console.log('FN4 exec.. ', bool2); return !bool2;}

console.log('Stack length on start:', fnStack.length);

fnStack.push(fn1);

console.log('Stack length after adding fn1:', fnStack.length);

fnStack.push(fn2);
fnStack.push(fn3);
fnStack.push(fn4);

console.log('Stack length after adding 3 more functions:', fnStack.length);



fnStack.executeStack(true, false);
console.log('Stack length after adding executing with (false, false):', fnStack.length);


document.write(fnStack.length);
